package com.atlassian.plugins.issuestatusdemo.servlets;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple servlet showing usage of Issue Status Plugin
 *
 * @since v6.1
 */
public class StatusTemplateServlet extends HttpServlet
{
    private static final Logger logger = LoggerFactory.getLogger(StatusTemplateServlet.class);
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final WebResourceManager webResourceManager;

    private static final String RESOURCES_KEY = "com.atlassian.plugins.issue-status-demo-plugin:issue-status-demo-resources";

    public StatusTemplateServlet(SoyTemplateRenderer soyTemplateRenderer, WebResourceManager webResourceManager)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.webResourceManager = webResourceManager;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException
    {
        response.setContentType("text/html");

        try
        {
            // setting resources
            StringWriter resources = new StringWriter();
            webResourceManager.requireResource(RESOURCES_KEY);
            webResourceManager.includeResources(resources, UrlMode.ABSOLUTE);

            // params for template
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("resourcesContent", resources.toString());
            data.put("statuses", getStatuses());

            // render template
            soyTemplateRenderer.render(response.getWriter(), RESOURCES_KEY, "IssueStatusDemo.testStatusTemplate", data);

        }
        catch (SoyException e)
        {
            logger.error("Cannot render soy template", e);
        }

        response.getWriter().close();
    }

    private List<Map<String, Object>> getStatuses()
    {
        // list of statuses
        List<Map<String, Object>> statuses = new ArrayList<Map<String, Object>>();

        // status 1
        Map<String, Object> category1 = new HashMap<String, Object>();
        category1.put("key", "key-green");
        category1.put("colorName", "green");

        Map<String, Object> status1 = new HashMap<String, Object>();
        status1.put("name", "Green status");
        status1.put("description", "Description of green status");
        status1.put("statusCategory", category1);
        statuses.add(status1);

        // status 2
        Map<String, Object> category2 = new HashMap<String, Object>();
        category2.put("key", "key-yellow");
        category2.put("colorName", "yellow");

        Map<String, Object> status2 = new HashMap<String, Object>();
        status2.put("name", "Yellow status");
        status2.put("description", "Description of yellow status");
        status2.put("statusCategory", category2);
        statuses.add(status2);

        // status 3
        Map<String, Object> category3 = new HashMap<String, Object>();
        category3.put("key", "key-brown");
        category3.put("colorName", "brown");

        Map<String, Object> status3 = new HashMap<String, Object>();
        status3.put("name", "Brown status");
        status3.put("description", "Description of brown status");
        status3.put("statusCategory", category3);
        statuses.add(status3);

        // status 4
        Map<String, Object> category4 = new HashMap<String, Object>();
        category4.put("key", "key-medium-gray");
        category4.put("colorName", "medium-gray");

        Map<String, Object> status4 = new HashMap<String, Object>();
        status4.put("name", "Medium gray status");
        status4.put("description", "Description of medium gray status");
        status4.put("statusCategory", category4);
        statuses.add(status4);

        // status 5
        Map<String, Object> category5 = new HashMap<String, Object>();
        category5.put("key", "key-warm-red");
        category5.put("colorName", "warm-red");

        Map<String, Object> status5 = new HashMap<String, Object>();
        status5.put("name", "Warm red status");
        status5.put("description", "Description of warm red status");
        status5.put("statusCategory", category5);
        statuses.add(status5);

        // status 6
        Map<String, Object> category6 = new HashMap<String, Object>();
        category6.put("key", "key-blue-gray");
        category6.put("colorName", "blue-gray");

        Map<String, Object> status6 = new HashMap<String, Object>();
        status6.put("name", "Blue gray status");
        status6.put("description", "Description of blue gray status");
        status6.put("statusCategory", category6);
        statuses.add(status6);

        // status 7
        Map<String, Object> category7 = new HashMap<String, Object>();
        category7.put("key", "key-other");
        category7.put("colorName", "other");

        Map<String, Object> status7 = new HashMap<String, Object>();
        status7.put("name", "Other status");
        status7.put("description", null);
        status7.put("statusCategory", category7);
        statuses.add(status7);

        return statuses;
    }
}
